/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pinblock;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author gosorio
 */
public class Generator {
    public static byte[] concat(byte[] a, byte[] b) {
        byte [] temp = new byte[a.length + b.length];
        System.arraycopy(a, 0, temp, 0, a.length);
        System.arraycopy(b, 0, temp, a.length, b.length);
        return temp;
    }

    public static byte[] getPinBlockISO_0(String PAN, String PIN) throws Exception {
        PAN = PAN.substring(PAN.length() - 12 - 1, PAN.length() - 1);
        String PaddingPAN = "0000".concat(PAN);
        
        String Fs = "FFFFFFFFFFFFFFFF";
        String PaddingPIN = "0"+PIN.length()+PIN + Fs.substring(2+PIN.length(), Fs.length());
        
        byte [] PINBlock = xorBytes(ISOUtil.hex2byte(PaddingPAN), ISOUtil.hex2byte(PaddingPIN));
        
        return PINBlock;
    }
    
    public static byte [] xorBytes(byte [] a, byte [] b) throws Exception {
        if(a.length != b.length) {
            throw new Exception();
        }
        byte[] result = new byte[a.length];
        
        for(int i = 0; i<a.length; i++) {
            int r = 0;
            r = a[i]^b[i];
            r &= 0xFF;
            result[i] = (byte)r;
        }
        return result;
        
    }
    
    public static SecretKey create2TDESKey(String stringKey) throws Exception {
        if(stringKey.length() != 32) {
            throw new Exception("Double length TDES has incorrect lenght: "  +
                     stringKey.length() + ", must be 32");
        }
        
        //Padding the Key
        byte [] KeyBytes = ISOUtil.hex2byte(stringKey);
        byte [] KeyBytesPadding = new byte[8];
        System.arraycopy(KeyBytes, 0, KeyBytesPadding, 0, 8);
        KeyBytes = concat(KeyBytes, KeyBytesPadding);
        
        //Create key with TDESede Algorithm
        SecretKey secrectKey = new SecretKeySpec(KeyBytes, "DESede");
        
        return secrectKey;
    }
    
    public static String doFinal_2TDES(String data, SecretKey key, int mode)
            throws NoSuchAlgorithmException, NoSuchPaddingException,
            InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        
        //Create cipher in encrypt mode
        Cipher cipher_mode = Cipher.getInstance("DESede");
        cipher_mode.init(mode, key);
        
        Cipher cipher = Cipher.getInstance("DESede");
        cipher.init(Cipher.ENCRYPT_MODE, key);
        
        //Get padding the data
        byte [] paddingData = cipher.doFinal(
                ISOUtil.hex2byte("30303030303030303030303030303030"));
        
        //Get bytes of data
        byte [] dataBytes = ISOUtil.hex2byte(data);
        
        //Pad the data
        int bytesToPad = 24 - dataBytes.length%24;
        byte [] finalPaddingData = new byte[bytesToPad];
        System.arraycopy(paddingData, paddingData.length - bytesToPad,
                finalPaddingData, 0, bytesToPad);
        
        byte [] finalDataBytes = new byte[dataBytes.length + bytesToPad];
        System.arraycopy(dataBytes, 0, finalDataBytes, 0, dataBytes.length);
        System.arraycopy(finalPaddingData, 0, finalDataBytes, dataBytes.length,
                finalPaddingData.length);
        
        //do Ecrypt
        byte [] ecryptDataBytes = cipher_mode.doFinal(finalDataBytes);
        return ISOUtil.hexString(ecryptDataBytes);
    }
}
